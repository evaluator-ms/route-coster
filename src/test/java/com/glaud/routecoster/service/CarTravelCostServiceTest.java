package com.glaud.routecoster.service;

import java.time.Duration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.glaud.routecoster.config.RabbitConfig;
import com.glaud.routecoster.model.FuelQuantity;
import com.glaud.routecoster.model.FuelType;
import com.glaud.routecoster.model.RouteCost;
import com.glaud.routecoster.model.RouteDetails;
import com.glaud.routecoster.model.Status;
import com.glaud.routecoster.model.TransportType;

@ExtendWith(SpringExtension.class)
public class CarTravelCostServiceTest {

	private TravelCostService carTravelCostService;

	@Mock
	RouteService routeService;

	@Mock
	RabbitTemplate rabbitTemplate;

	private RouteCost routeCost;
	private RouteDetails routeDetails;

	@BeforeEach
	public void setUp() {
		carTravelCostService = new CarTravelCostService(routeService, rabbitTemplate);
	}

	private void setUpRouteDetails() {
		routeDetails = new RouteDetails.Builder("Gdynia, Poland", "Sopot, Poland")
				.distance(new Distance(new Double(10177) / 1000, Metrics.KILOMETERS)).duration(Duration.ofSeconds(990L))
				.status(Status.OK).build();
	}

	private void setUpRouteDetailsNotFound() {
		routeDetails = new RouteDetails(Status.NOT_FOUND);
	}

	@Test
	public void findTravelCostTest() throws Exception {
		setUpRouteDetails();
		String origin = "Gdynia";
		String destination = "Sopot";
		String carQuery = "audi/a3/8p/hatchback-3d/silnik-benzynowy-1.6-102km-2003-2010/";
		routeCost = new RouteCost(routeDetails, TransportType.CAR, 3.3004011, Status.OK);
		when(rabbitTemplate.convertSendAndReceiveAsType(RabbitConfig.EXCHANGE_FUEL, "rpc.fuel", carQuery,
				new ParameterizedTypeReference<FuelQuantity>() {
				})).thenReturn(new FuelQuantity(6.9, 6.8, FuelType.GASOLINE, Status.OK));
		when(routeService.findRouteDetails(origin, destination)).thenReturn(routeDetails);
		assertEquals(routeCost, carTravelCostService.findTravelCost(origin, destination, carQuery));
	}

	@Test
	public void findTravelCostNotFoundRouteTest() throws Exception {
		setUpRouteDetailsNotFound();
		String origin = "Gdynia";
		String destination = "Sopot";
		String carQuery = "audi/a3/8p/hatchback-3d/silnik-benzynowy-1.6-102km-2003-2010/";
		routeCost = new RouteCost(routeDetails, Status.NOT_FOUND);
		when(rabbitTemplate.convertSendAndReceiveAsType(RabbitConfig.EXCHANGE_FUEL, "rpc.fuel", carQuery,
				new ParameterizedTypeReference<FuelQuantity>() {
				})).thenReturn(new FuelQuantity(6.9, 6.8, FuelType.GASOLINE, Status.OK));
		when(routeService.findRouteDetails(origin, destination)).thenReturn(routeDetails);
		assertEquals(routeCost, carTravelCostService.findTravelCost(origin, destination, carQuery));
	}
	@Test
	public void findTravelCostNotFoundFuelTest() throws Exception {
		setUpRouteDetails();
		String origin = "Gdynia";
		String destination = "Sopot";
		String carQuery = "audi/a3/8p/hatchback-3d/silnik-benzynowy-1.6-102km-2003-2010/";
		routeCost = new RouteCost(Status.NOT_FOUND);
		when(rabbitTemplate.convertSendAndReceiveAsType(RabbitConfig.EXCHANGE_FUEL, "rpc.fuel", carQuery,
				new ParameterizedTypeReference<FuelQuantity>() {
		})).thenReturn(new FuelQuantity(Status.NOT_FOUND));
		when(routeService.findRouteDetails(origin, destination)).thenReturn(routeDetails);
		assertEquals(routeCost, carTravelCostService.findTravelCost(origin, destination, carQuery));
	}
}
