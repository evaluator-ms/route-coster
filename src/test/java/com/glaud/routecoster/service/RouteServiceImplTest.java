package com.glaud.routecoster.service;

import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.glaud.routecoster.model.RouteDetails;
import com.glaud.routecoster.model.Status;
import com.glaud.routecoster.service.RouteServiceImpl;

@ExtendWith(SpringExtension.class)
public class RouteServiceImplTest {

	@Spy
	RouteServiceImpl routeServiceImpl;

	private RouteDetails expectedRouteDetails;
	private ResponseEntity<String> response;

	private void setUpRouteDetailsResponse() {
		expectedRouteDetails = new RouteDetails.Builder("Gdynia, Poland", "Sopot, Poland")
				.distance(new Distance(new Double(10177) / 1000, Metrics.KILOMETERS)).duration(Duration.ofSeconds(990L))
				.status(Status.OK).build();
	}

	private void setUpNotFoundRouteDetailsResponse() {
		expectedRouteDetails = new RouteDetails(Status.NOT_FOUND);
	}

	private void setUpNotFoundJsonResponse() {
		String responseBody = "{\n" + "   \"destination_addresses\" : [ \"\" ],\n"
				+ "   \"origin_addresses\" : [ \"Gdynia, Polska\" ],\n" + "   \"rows\" : [\n" + "      {\n"
				+ "         \"elements\" : [\n" + "            {\n" + "               \"status\" : \"NOT_FOUND\"\n"
				+ "            }\n" + "         ]\n" + "      }\n" + "   ],\n" + "   \"status\" : \"OK\"\n" + "}";
		response = new ResponseEntity<>(responseBody, HttpStatus.OK);
	}

	private void setUpJsonResponse() {
		String responseBody = "{\"destination_addresses\" : [ \"Sopot, Poland\" ],\"origin_addresses\" : [ \"Gdynia, Poland\" ],\"rows\" : [{"
				+ "\"elements\" : [{\"distance\" : {\"text\" : \"10.2 km\",\"value\" : 10177"
				+ "},\"duration\" : {\"text\" : \"17 mins\",\"value\" : 990},"
				+ "\"status\" : \"OK\"}]}],\"status\" : \"OK\"}";
		response = new ResponseEntity<>(responseBody, HttpStatus.OK);
	}

	@Test
	public void findRouteDetailsTest() {
		setUpRouteDetailsResponse();
		setUpJsonResponse();
		String origin = "Gdynia";
		String destination = "Sopot";
		doReturn(response).when(routeServiceImpl).invokeDistanceMatrixApi(origin, destination);
		when(routeServiceImpl.invokeDistanceMatrixApi(origin, destination)).thenReturn(response);
		assertEquals(expectedRouteDetails, routeServiceImpl.findRouteDetails(origin, destination));
	}

	@Test
	public void findRouteDetailsErrorTest() {
		setUpNotFoundRouteDetailsResponse();
		setUpNotFoundJsonResponse();
		String origin = "Gdynia";
		String destination = "sfdsfd";
		doReturn(response).when(routeServiceImpl).invokeDistanceMatrixApi(origin, destination);
		when(routeServiceImpl.invokeDistanceMatrixApi(origin, destination)).thenReturn(response);
		assertEquals(expectedRouteDetails, routeServiceImpl.findRouteDetails(origin, destination));
	}

}
