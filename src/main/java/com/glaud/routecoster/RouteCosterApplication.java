package com.glaud.routecoster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Import;

import com.glaud.routecoster.config.AppConfig;
import com.glaud.routecoster.config.RabbitConfig;

@SpringBootApplication
@Import({ AppConfig.class, RabbitConfig.class })
@EnableEurekaClient
public class RouteCosterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RouteCosterApplication.class, args);
	}

}
