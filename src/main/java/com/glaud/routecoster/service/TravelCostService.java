package com.glaud.routecoster.service;

import com.glaud.routecoster.model.RouteCost;

public interface TravelCostService {

	RouteCost findTravelCost(String originAddress, String destinationAddress, String carQuery) throws Exception;

}
