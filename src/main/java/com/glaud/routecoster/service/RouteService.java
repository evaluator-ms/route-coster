package com.glaud.routecoster.service;

import org.springframework.http.ResponseEntity;

import com.glaud.routecoster.model.RouteDetails;

public interface RouteService {

	RouteDetails findRouteDetails(String originAddress, String destinationAddress);

	ResponseEntity<String> invokeDistanceMatrixApi(String originAddress, String destinationAddress);

}
