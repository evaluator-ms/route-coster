package com.glaud.routecoster.service;

import java.math.BigInteger;
import java.time.Duration;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.glaud.routecoster.model.RouteDetails;
import com.glaud.routecoster.model.Status;

@Service
public class RouteServiceImpl implements RouteService {

	@Value("${google.maps.api.url}")
	private String url;

	@Value("${google.maps.api.key}")
	private String apiKey;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public RouteDetails findRouteDetails(String originAddress, String destinationAddress) {
		ResponseEntity<String> response = invokeDistanceMatrixApi(originAddress, destinationAddress);
		if (!response.getStatusCode().equals(HttpStatus.OK)) {
			return new RouteDetails(Status.ERROR);
		}
		JSONObject jObj = new JSONObject(response.getBody());
		JSONObject elements = jObj.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0);
		if (!elements.getString("status").equals("OK")) {
			return new RouteDetails(Status.NOT_FOUND);
		}
		RouteDetails routeDetails = new RouteDetails.Builder(jObj.getJSONArray("origin_addresses").getString(0),
				jObj.getJSONArray(("destination_addresses")).getString(0))
						.distance(parseDistance(elements.getJSONObject("distance")))
						.duration(parseDuration(elements.getJSONObject("duration"))).status(Status.OK).build();
		return routeDetails;
	}

	@Override
	public ResponseEntity<String> invokeDistanceMatrixApi(String originAddress, String destinationAddress) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("origins", originAddress)
				.queryParam("destinations", destinationAddress).queryParam("key", apiKey);
		HttpEntity<?> entity = new HttpEntity<>(headers);
		ResponseEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
				entity, String.class);
		return response;
	}

	private Duration parseDuration(JSONObject jsonObject) {
		BigInteger seconds = jsonObject.getBigInteger("value");
		return Duration.ofSeconds(seconds.longValue());
	}

	private Distance parseDistance(JSONObject jsonObject) {
		BigInteger meters = jsonObject.getBigInteger("value");
		return new Distance(meters.doubleValue() / 1000, Metrics.KILOMETERS);
	}

}
