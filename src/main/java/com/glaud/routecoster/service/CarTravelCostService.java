package com.glaud.routecoster.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import com.glaud.routecoster.config.RabbitConfig;
import com.glaud.routecoster.model.FuelQuantity;
import com.glaud.routecoster.model.FuelType;
import com.glaud.routecoster.model.RouteCost;
import com.glaud.routecoster.model.RouteDetails;
import com.glaud.routecoster.model.Status;
import com.glaud.routecoster.model.TransportType;

@Service
public class CarTravelCostService implements TravelCostService {

	private RouteService routeService;
	private RabbitTemplate rabbitTemplate;

	@Autowired
	public CarTravelCostService(RouteService routeService, RabbitTemplate rabbitTemplate) {
		this.routeService = routeService;
		this.rabbitTemplate = rabbitTemplate;
	}

	@Override
	public RouteCost findTravelCost(String originAddress, String destinationAddress, String carQuery) throws Exception {
		RouteDetails routeDetails = routeService.findRouteDetails(originAddress, destinationAddress);
		if (!routeDetails.getStatus().equals(Status.OK)) {
			return new RouteCost(routeDetails, Status.NOT_FOUND);
		}
		FuelQuantity fuelQuantity = rabbitTemplate.convertSendAndReceiveAsType(RabbitConfig.EXCHANGE_FUEL, "rpc.fuel",
				carQuery, new ParameterizedTypeReference<FuelQuantity>() {
				});
		if (!fuelQuantity.getStatus().equals(Status.OK)) {
			return new RouteCost(Status.NOT_FOUND);
		}
		Double fuelPrice = getFuelPrice(fuelQuantity.getFuelType());
		Double cost = routeDetails.getDistance().getValue() * fuelQuantity.getAvgConsumption() / 100 * fuelPrice;
		return new RouteCost(routeDetails, TransportType.CAR, cost, Status.OK);
	}

	private Double getFuelPrice(FuelType fuelType) {
		if (fuelType.equals(FuelType.GASOLINE)) {
			return new Double(4.70);
		} else if (fuelType.equals(FuelType.DIESEL)) {
			return new Double(4.90);
		} else {
			return new Double(5);
		}
	}

}
