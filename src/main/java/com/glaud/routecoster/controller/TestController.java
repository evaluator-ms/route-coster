package com.glaud.routecoster.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.glaud.routecoster.model.RequestedCar;
import com.glaud.routecoster.model.RouteCost;
import com.glaud.routecoster.model.RouteDetails;
import com.glaud.routecoster.service.RouteService;
import com.glaud.routecoster.service.TravelCostService;

@RestController
public class TestController {

	@Autowired
	RouteService routeService;

	@Autowired
	TravelCostService travelCostService;

	@GetMapping("/getRouteDetails")
	public RouteDetails getRouteDetails(@RequestParam("origin") String originAddress,
			@RequestParam("destination") String destinationAddress) {
		System.out.println(originAddress);
		System.out.println(destinationAddress);
		return routeService.findRouteDetails(originAddress, destinationAddress);
	}

	@GetMapping("/getCar")
	public RequestedCar getCar() {
		return new RequestedCar.Builder("audi", "a3", "silnik-benzynowy-1.6-102km").generation("8p")
				.version("hatchback-3d").build();
	}

	@GetMapping("/findTravelCostTest")
	public RouteCost findTravelCost(@RequestParam("origin") String originAddress,
			@RequestParam("destination") String destinationAddress) throws Exception {
		return travelCostService.findTravelCost(originAddress, destinationAddress,
				"audi/a3/8p/hatchback-3d/silnik-benzynowy-1.6-102km-2003-2010/");
	}
}
