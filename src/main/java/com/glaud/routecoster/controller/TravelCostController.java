package com.glaud.routecoster.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.glaud.routecoster.model.RouteCost;
import com.glaud.routecoster.model.Status;
import com.glaud.routecoster.model.TravelCostRequest;
import com.glaud.routecoster.service.TravelCostService;

@Controller
public class TravelCostController {

	@Autowired
	private TravelCostService travelCostService;

	@PostMapping("/findTravelCost")
	@ResponseBody
	public RouteCost findTravelCost(@RequestBody TravelCostRequest travelCostRequest) {
		System.out.println("travelCostRequest: " + travelCostRequest);
		try {
			return travelCostService.findTravelCost(travelCostRequest.getOrigin(), travelCostRequest.getDestination(),
					travelCostRequest.getCarQuery());
		} catch (Exception e) {
			e.printStackTrace();
			return new RouteCost(Status.NOT_FOUND);
		}
	}

}
