package com.glaud.routecoster.model;

public enum FuelType {
	GASOLINE, DIESEL, LPG, UNKNOWN

}
