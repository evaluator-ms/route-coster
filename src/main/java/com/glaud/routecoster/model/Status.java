package com.glaud.routecoster.model;

public enum Status {
	OK, NOT_FOUND, ERROR
}