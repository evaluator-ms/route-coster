package com.glaud.routecoster.model;

import lombok.Data;

@Data
public class TravelCostRequest {
	
	private String origin;
	private String destination;
	private String carQuery;
	
	public TravelCostRequest(String origin, String destination, String carQuery) {
		this.origin = origin;
		this.destination = destination;
		this.carQuery = carQuery;
	}
}
