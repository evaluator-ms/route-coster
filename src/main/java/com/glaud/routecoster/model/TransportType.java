package com.glaud.routecoster.model;

public enum TransportType {
	CAR, PUBLIC_TRANSPORT, TAXI, UBER
}
