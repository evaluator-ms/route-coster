package com.glaud.routecoster.model;

import java.time.Duration;

import org.springframework.data.geo.Distance;

import lombok.Data;

@Data
public class RouteDetails {
	public static RouteDetails NOT_FOUND = new RouteDetails(Status.NOT_FOUND);

	private RouteDetails() {
	}

	public RouteDetails(Status status) {
		this.status = status;
	}

	private String originAddress;
	private String destinationAddress;
	private Distance distance;
	private Duration duration;
	private Status status;

	public static class Builder {
		private String originAddress;
		private String destinationAddress;
		private Distance distance;
		private Duration duration;
		private Status status;

		public Builder(String originAddress, String destinationAddress) {
			this.originAddress = originAddress;
			this.destinationAddress = destinationAddress;
		}

		public Builder distance(Distance distance) {
			this.distance = distance;
			return this;
		}

		public Builder duration(Duration duration) {
			this.duration = duration;
			return this;
		}

		public Builder status(Status status) {
			this.status = status;
			return this;
		}

		public RouteDetails build() {
			RouteDetails routeDetails = new RouteDetails();
			routeDetails.originAddress = this.originAddress;
			routeDetails.destinationAddress = this.destinationAddress;
			routeDetails.distance = this.distance;
			routeDetails.duration = this.duration;
			routeDetails.status = this.status;
			return routeDetails;
		}
	}

}
