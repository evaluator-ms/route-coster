package com.glaud.routecoster.model;

import lombok.Data;

@Data
public class RouteCost {

	public static RouteCost NOT_FOUND = new RouteCost(Status.NOT_FOUND);

	private RouteDetails routeDetails;
	private TransportType transportType;
	private Double cost;
	private Status status;

	public RouteCost(RouteDetails routeDetails, TransportType transportType, Double cost, Status status) {
		this.routeDetails = routeDetails;
		this.transportType = transportType;
		this.cost = cost;
		this.status = status;
	}

	public RouteCost(Status status) {
		this.status = status;
	}
	
	public RouteCost(RouteDetails routeDetails, Status status) {
		this.routeDetails = routeDetails;
		this.status = status;
	}
}
