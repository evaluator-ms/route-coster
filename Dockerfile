FROM openjdk:8-jdk-alpine
LABEL maintainer="Glaud"
RUN apk --no-cache add netcat-openbsd
VOLUME /tmp
EXPOSE 9997
ARG JAR_FILE=/target/route-coster-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} route-coster.jar
COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]